#!/bin/bash

# NOTE: I might need to install gtk3 to get the GUI running correctly.

set -eo pipefail

mkdir -p /tmp/emacs-build && cd /tmp/emacs-build

git clone --depth=1 https://github.com/emacs-mirror/emacs.git

cd emacs

./autogen.sh
# Emacs adds its own bin/ to the end of the prefix, so we omit it here to avoid
# /usr/local/bin/bin/emacs
./configure --prefix=/usr/local/

make -j8 && make -j8 install
